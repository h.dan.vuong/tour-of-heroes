import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
import {public_key} from "./Marvel";

Vue.use(Vuex)

export const store =  new Vuex.Store({
    state: {
        heroes: [],
        displayCard: true,
        orderSelection: 2,
        maxHeroesDisplayed: 5,
    },
    mutations: {
        ADD_HERO: function (state, item){
            state.heroes.push(item)
        },
        SET_FAV_HERO: function(state, item){
            let hero = state.heroes.find( hero =>{
                 return hero.id === item.id
            })
            hero.isFav = true
        },
        ADD_NEW_HERO: function(state, p_hero){
            state.heroes.unshift(p_hero)
        },
        DELETE_HERO: function(state, p_index){
            this.state.heroes.splice(p_index, 1)
        },
        UNSET_FAV_HERO: function(state, fav){
            let hero = state.heroes.find(hero=>{
                return hero.id === fav.id
            })
            hero.isFav = false
        },
        SET_MAX_HEROES_DISPLAYED: function(state, value){
          state.maxHeroesDisplayed = value
        },
        TOGGLE_DISPLAY_CARD: function(state, value){
          state.displayCard = value
        },
        TOGGLE_ORDER_SELECTION: function(state, value){
          state.orderSelection = value
        },
        UPDATE_DESCRIPTION: function(state, data){
            let hero = state.heroes.find((hero) => {
                return hero.id === data.id
            })
            hero.description = data.description
        },
        UPDATE_NAME: function (state, data) {
            let hero = state.heroes.find((hero) => {
                return hero.id === data.id
            })
            hero.name = data.name
        }
    },
    actions: {
        AddFavHero({commit}, item)  {
            commit('SET_FAV_HERO', item)
        }
        ,AddNewHero({commit}, item)  {
            commit('ADD_NEW_HERO', item)
        },
        DeleteFavHero({commit}, fav){
          commit('UNSET_FAV_HERO', fav)
        },
        GetHeroes: async function({commit}, offset){
            await axios.get('https://gateway.marvel.com:443/v1/public/characters?offset='+offset+'&limit=22&apikey='+public_key)
              .then((result)=>{
                  result.data.data.results.forEach((item)=>{
                      item.path = item.thumbnail.path +"."+ item.thumbnail.extension
                      item.isFav = false
                      commit('ADD_HERO', item)
                  })
              })
              .catch((error)=>{
                  console.log(error)
              })
        },
        UpdateInfo({commit}, data){
            commit('UPDATE_DESCRIPTION', data)
            commit('UPDATE_NAME', data)
        },
        DeleteHero({commit}, p_hero){
            console.log(p_hero.name)
            const index = this.state.heroes.findIndex( hero => {
                return hero.id === p_hero.id
            })
            console.log(index)
            if (index != -1){
                commit('DELETE_HERO', index)
            }
        }
    },
    getters: {
        favHeroes: (state) => {
           return state.heroes.filter(hero => hero.isFav)
        },
        nameFilter: (state) => {
            return (nameSearch, idSearch) => {
                return state.heroes.filter((hero) => {
                    if (nameSearch !== null) {
                        return (hero.name.toLowerCase().includes(nameSearch.toLowerCase()))
                    }
                    else{
                        return true
                    }
                })
                    .filter(hero => {
                        if (idSearch !== null) {
                            return hero.id.toString().includes(idSearch)
                        }
                        else{
                            return true
                        }
                    })

            };
        },
        nbrOfHeroes: (state) => {
            return state.heroes.length
        }
    }
})