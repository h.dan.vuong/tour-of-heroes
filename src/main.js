import Vue from 'vue'
import App from './App.vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import { dom } from '@fortawesome/fontawesome-svg-core'
import vuetify from './plugins/vuetify';
import {store} from './store'
import VueRouter from "vue-router";
import Heroes from "./components/Heroes";
import Dashboard from "./components/Dashboard";
import AddHeroComponent from "./components/AddHeroComponent";
import EditHero from "./components/EditHero";

Vue.use(VueRouter)

dom.watch()

const router = new VueRouter({
  routes: [
    { path: '/', component: Heroes , meta:{title: 'Heroes'}},
    { path: '/favorites', component: Dashboard , meta:{title: 'Favorites'}},
    { path: '/add', component: AddHeroComponent , meta:{title: 'Add Hero'}},
    { path: '/edit', component: EditHero,name:"EDIT",props: true , meta:{title: 'Edit Hero'}},
  ]
})

Vue.config.productionTip = false
new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
